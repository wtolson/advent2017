#!/usr/bin/env swipl -s

parse_file(Filename, Passphrases) :-
    open(Filename, read, Stream),
    parse_stream(Stream, Passphrases),
    close(Stream).

parse_stream(Stream, []) :-
    at_end_of_stream(Stream).

parse_stream(Stream, [Jump | Tail]) :-
    read_string(Stream, "\n", "\r", _, Line),
    number_codes(Jump, Line),
    parse_stream(Stream, Tail).

execute_jump(Instructions, Pos, NextInstructions, NextPos) :-
    length(Left, Pos),
    append(Left, [Jump | Right], Instructions),
    NextJump is Jump + 1,
    append(Left, [NextJump | Right], NextInstructions),
    NextPos is Pos + Jump.

count_jumps(Instructions, Jumps) :-
    count_jumps(Instructions, 0, 0, Jumps).

count_jumps(Instructions, Pos, Accumulator, Accumulator) :-
    Pos < 0;
    length(Instructions, Len), Pos >= Len.

count_jumps(Instructions, Pos, Accumulator, Jumps) :-
    NextAccumulator is Accumulator + 1,
    execute_jump(Instructions, Pos, NextInstructions, NextPos),
    count_jumps(NextInstructions, NextPos, NextAccumulator, Jumps).

main :-
    parse_file("input.txt", Instructions),
    count_jumps(Instructions, Jumps),
    writeln(Jumps).

:- initialization(main, main).
