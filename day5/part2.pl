#!/usr/bin/env swipl -G8g -L8g -s
:- use_module(library(clpfd)).

parse_file(Filename, Passphrases) :-
    open(Filename, read, Stream),
    parse_stream(Stream, Passphrases),
    close(Stream).

parse_stream(Stream, []) :-
    at_end_of_stream(Stream).

parse_stream(Stream, [Jump | Tail]) :-
    read_string(Stream, "\n", "\r", _, Line),
    number_codes(Jump, Line),
    parse_stream(Stream, Tail).

next_jump(Jump, NextJump) :-
    Jump #>= 3, NextJump #= Jump - 1;
    NextJump #= Jump + 1.

execute_jump(Pos, Instructions, NextPos, NextInstructions) :-
    execute_jump(0, Instructions, Pos, NextInstructions, NextPos).

execute_jump(Pos, Pos, [Jump | Tail], NextPos, [NextJump | Tail]) :-
    next_jump(Jump, NextJump),
    NextPos #= Pos + Jump.

execute_jump(Index, Pos, [Head | Tail], NextPos, [Head | NextTail]) :-
    Index #< Pos,
    NextIndex #= Index + 1,
    execute_jump(NextIndex, Pos, Tail, NextPos, NextTail).

count_jumps(Instructions, Jumps) :-
    count_jumps(Instructions, 0, 0, Jumps).

count_jumps(Instructions, Pos, Accumulator, Accumulator) :-
    Pos #< 0;
    length(Instructions, Len), Pos #>= Len.

count_jumps(Instructions, Pos, Accumulator, Jumps) :-
    NextAccumulator #= Accumulator + 1,
    execute_jump(Instructions, Pos, NextInstructions, NextPos),
    count_jumps(NextInstructions, NextPos, NextAccumulator, Jumps).

main :-
    parse_file("input.txt", Instructions),
    count_jumps(Instructions, Jumps),
    writeln(Jumps).

:- initialization(main, main).
