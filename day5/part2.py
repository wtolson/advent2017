def count_jumps(instructions):
    result = 0
    pos = 0
    instructions = list(instructions)

    while pos >= 0 and pos < len(instructions):
        jump = instructions[pos]

        if jump >= 3:
            instructions[pos] -= 1
        else:
            instructions[pos] += 1

        pos += jump
        result += 1

    return result


def main():
    with open('input.txt') as fp:
        instructions = [int(i, 10) for i in fp.read().split()]
    print count_jumps(instructions)


if __name__ == '__main__':
    main()
