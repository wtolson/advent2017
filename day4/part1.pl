#!/usr/bin/env swipl -s

has_duplicates(Words) :-
    append(_, [Word | Rest], Words),
    memberchk(Word, Rest).

valid_passphrases(Passphrases, Result) :-
    valid_passphrases(Passphrases, 0, Result).

valid_passphrases([], Accumulator, Accumulator).
valid_passphrases([Passphrase | Passphrases], Accumulator, Result) :-
    has_duplicates(Passphrase),
        valid_passphrases(Passphrases, Accumulator, Result);
    NewAccumulator is Accumulator + 1,
    valid_passphrases(Passphrases, NewAccumulator, Result).

parse_file(Filename, Passphrases) :-
    open(Filename, read, Stream),
    parse_stream(Stream, Passphrases),
    close(Stream).

parse_stream(Stream, []) :-
    at_end_of_stream(Stream).

parse_stream(Stream, [Words | Tail]) :-
    read_string(Stream, "\n", "\r", _, Line),
    split_string(Line, " ", "", Words),
    parse_stream(Stream, Tail).

main :-
    parse_file("input.txt", Passphrases),
    valid_passphrases(Passphrases, Result),
    write(Result), nl.

:- initialization(main, main).
