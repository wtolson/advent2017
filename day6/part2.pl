#!/usr/bin/env swipl -s
:- use_module(library(clpfd)).

parse_file(Filename, Memory) :-
    open(Filename, read, Stream),
    parse_stream(Stream, Memory),
    close(Stream).

parse_stream(Stream, Memory) :-
    read_string(Stream, "\n", "\r", _, Line),
    split_string(Line, "\t ", "", RowStrings),
    maplist(number_codes, Memory, RowStrings).

replace(List, Index, Elem, NewList) :-
    replace(List, 0, Index, Elem, NewList).

replace([_|List], Index, Index, Elem, [Elem|List]).
replace([E|List], Index, End, Elem, [E|NewList]) :-
    Index #< End,
    NextIndex #= Index+1,
    replace(List, NextIndex, End, Elem, NewList).

next_index(List, Index, NextIndex) :-
    length(List, Length),
    NextIndex is (Index + 1) mod Length.

next_elem(List, Index, NextElem) :-
    nth0(Index, List, Elem),
    NextElem is Elem + 1.

max_index(Memory, Max, Index) :-
    max_list(Memory, Max),
    nth0(Index, Memory, Max).

distribute(Memory, NewMemory) :-
    max_index(Memory, Max, Index),
    replace(Memory, Index, 0, NextMemory),
    next_index(Memory, Index, NextIndex),
    distribute(Max, NextIndex, NextMemory, NewMemory).

distribute(0, _, Memory, Memory).

distribute(Blocks, Index, Memory, NewMemory) :-
    next_index(Memory, Index, NextIndex),
    next_elem(Memory, Index, NextElem),
    replace(Memory, Index, NextElem, NextMemory),
    NextBlocks is Blocks - 1,
    distribute(NextBlocks, NextIndex, NextMemory, NewMemory).

count_steps(Memory, Steps) :-
    count_steps(Memory, [], Steps).

count_steps(Memory, History, Steps) :-
    nth1(Steps, History, Memory).

count_steps(Memory, History, Steps) :-
    distribute(Memory, NewMemory),
    count_steps(NewMemory, [Memory | History], Steps).

main :-
    parse_file("input.txt", Memory),
    count_steps(Memory, Steps),
    writeln(Steps).

:- initialization(main, main).
