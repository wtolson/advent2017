#!/usr/bin/env swipl -s

parseFile(Filename, Numbers) :-
    open(Filename, read, Stream),
    parseStream(Stream, Numbers),
    close(Stream).

mapNumbers([], []).
mapNumbers([HeadIn | TailIn], [HeadOut | TailOut]) :-
    number_codes(HeadOut, HeadIn),
    mapNumbers(TailIn, TailOut).

parseLine(Line, Row) :-
    split_string(Line, "\t ", "", RowStrings),
    mapNumbers(RowStrings, Row).

parseStream(Stream, []) :-
    at_end_of_stream(Stream).

parseStream(Stream, [Row | Tail]) :-
    read_string(Stream, "\n", "\r", _, Line),
    parseLine(Line, Row),
    parseStream(Stream, Tail).

checksumRow(Row, Sum) :-
    member(A, Row),
    member(B, Row),
    A \= B,
    0 is A mod B,
    Sum is A / B.

checksumRows([], []).
checksumRows([Row | Spreedsheet], [Sum | Result]) :-
    checksumRow(Row, Sum),
    checksumRows(Spreedsheet, Result).

checksum(Spreedsheet, Result) :-
    checksumRows(Spreedsheet, RowSums),
    sumlist(RowSums, Result).

main :-
    parseFile("input.txt", Spreedsheet),
    checksum(Spreedsheet, Result),
    write(Result), nl.

:- initialization(main, main).
