#!/usr/bin/env swipl -s
:- use_module(library(assoc)).

parse_file(Filename, Instructions) :-
    open(Filename, read, Stream),
    parse_stream(Stream, Instructions),
    close(Stream).

parse_stream(Stream, []) :-
    at_end_of_stream(Stream).

parse_stream(Stream, [Instruction | Instructions]) :-
    read_string(Stream, "\n", "\r", _, Line),
    split_string(Line, " ", " ", Instruction),
    parse_stream(Stream, Instructions).


get_assoc_default(Key, Assoc, Value, Default) :-
    get_assoc(Key, Assoc, Value), !;
    Value is Default.

compare_values(A, ">", B) :- A > B.
compare_values(A, "<", B) :- A < B.
compare_values(A, "<=", B) :- A =< B.
compare_values(A, ">=", B) :- A >= B.
compare_values(A, "==", B) :- A == B.
compare_values(A, "!=", B) :- A =\= B.

exec_instruction("inc", Value, CurrentValue, NextValue) :-
    NextValue is CurrentValue + Value.

exec_instruction("dec", Value, CurrentValue, NextValue) :-
    NextValue is CurrentValue - Value.

run_instruction([Reg, Op, ValueStr, "if", CompReg, Comp, CompValueStr], State, NextState) :-
    get_assoc_default(CompReg, State, CompRegValue, 0),
    number_codes(CompValue, CompValueStr),
    compare_values(CompRegValue, Comp, CompValue),
    get_assoc_default(Reg, State, CurrentValue, 0),
    number_codes(Value, ValueStr),
    exec_instruction(Op, Value, CurrentValue, NextValue),
    put_assoc(Reg, State, NextValue, NextState).

run_instruction(_, State, State).

run(Instructions, Result) :-
    empty_assoc(State),
    run(Instructions, State, Result).

run([], State, Result) :-
    assoc_to_values(State, Values),
    max_list(Values, Result).

run([Instruction | Instructions], State, Result) :-
    run_instruction(Instruction, State, NextState),
    run(Instructions, NextState, Result).

main :-
    parse_file("input.txt", Instructions),
    run(Instructions, Result),
    writeln(Result).

:- initialization(main, main).
