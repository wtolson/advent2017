splitList(List, FirstHalf, SecondHalf) :-
    append(FirstHalf, SecondHalf, List),
    length(FirstHalf, N),
    length(SecondHalf, N).

captcha(List, Sum) :-
    splitList(List, FirstHalf, SecondHalf),
    captcha(FirstHalf, SecondHalf, 0, Sum).

% Our terminator
captcha([], [], Accumulator, Accumulator).

% If the head is the same...
captcha([Head | TailOne], [Head | TailTwo], Accumulator, Sum) :-
    NewAccumulator is Accumulator + Head + Head,  % We account for the head twice
    captcha(TailOne, TailTwo, NewAccumulator, Sum).

% If the head is the different...
captcha([_ | TailOne], [_ | TailTwo], Accumulator, Sum) :-
    captcha(TailOne, TailTwo, Accumulator, Sum).

% Parsing the input
parse(Stream, []) :-
    peek_char(Stream, '\n');
    at_end_of_stream(Stream).

parse(Stream, [H | T]) :-
    \+  at_end_of_stream(Stream),
    get_char(Stream, C),
    number_codes(H, [C]),
    parse(Stream, T).

main :-
    open("input.txt", read, InputStream),
    parse(InputStream, Numbers),
    captcha(Numbers, Result),
    write(Result), nl,
    close(InputStream).
