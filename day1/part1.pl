matchesNext(H, [H | _]).

captcha(List, Sum) :-
    [First | _] = List,
    append(List, [First], CircularList),
    captcha(CircularList, 0, Sum).

captcha([], Accumulator, Accumulator).

captcha([Head | Tail], Accumulator, Result) :-
    matchesNext(Head, Tail),
        NewAccumulator is Accumulator + Head,
        captcha(Tail, NewAccumulator, Result);
    captcha(Tail, Accumulator, Result).

parse(Stream, []) :-
    peek_char(Stream, '\n');
    at_end_of_stream(Stream).

parse(Stream, [H | T]) :-
    \+  at_end_of_stream(Stream),
    get_char(Stream, C),
    number_codes(H, [C]),
    parse(Stream, T).

main :-
    open("input.txt", read, InputStream),
    parse(InputStream, Numbers),
    captcha(Numbers, Result),
    write(Result), nl,
    close(InputStream).
