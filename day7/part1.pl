#!/usr/bin/env swipl -s

:- dynamic
    child/2.

load_file(Filename) :-
    open(Filename, read, Stream),
    load_stream(Stream),
    close(Stream).

load_stream(Stream) :-
    at_end_of_stream(Stream).

load_stream(Stream) :-
    read_string(Stream, "\n", "\r", _, Line),
    split_string(Line, "-", ">", Row),
    load_row(Row),
    load_stream(Stream).

load_row([_ | []]).
load_row([Node, ChildrenStr]) :-
    split_string(Node, " ", " ", [ParentName, _]),
    split_string(ChildrenStr, ",", " ", ChildrenNames),
    atom_codes(Parent, ParentName),
    maplist(atom_codes, Children, ChildrenNames),
    assert_children(Parent, Children).

assert_children(_, []).
assert_children(Parent, [Child | Children]) :-
    assertz(child(Parent, Child)),
    assert_children(Parent, Children).

root(Root) :-
    child(Root, _),
    \+ child(_, Root).

main :-
    load_file("input.txt"),
    root(Root),
    writeln(Root).

:- initialization(main, main).
