#!/usr/bin/env swipl -s

:- dynamic
    child/2,
    weight/2.

load_file(Filename) :-
    open(Filename, read, Stream),
    load_stream(Stream),
    close(Stream).

load_stream(Stream) :-
    at_end_of_stream(Stream).

load_stream(Stream) :-
    read_string(Stream, "\n", "\r", _, Line),
    split_string(Line, "-", ">", Row),
    load_row(Row),
    load_stream(Stream).

load_row([Node | []]) :-
    split_string(Node, " ", " ()", [ParentName, WeightStr]),
    atom_codes(Parent, ParentName),
    number_codes(Weight, WeightStr),
    assertz(weight(Parent, Weight)).

load_row([Node, ChildrenStr]) :-
    split_string(Node, " ", " ()", [ParentName, WeightStr]),
    split_string(ChildrenStr, ",", " ", ChildrenNames),
    atom_codes(Parent, ParentName),
    number_codes(Weight, WeightStr),
    maplist(atom_codes, Children, ChildrenNames),
    assertz(weight(Parent, Weight)),
    assert_children(Parent, Children).

assert_children(_, []).
assert_children(Parent, [Child | Children]) :-
    assertz(child(Child, Parent)),
    assert_children(Parent, Children).

all_children(Parent, Children) :-
    findall(Child, child(Child, Parent), Children).

children_weight(Node, Weight) :-
    all_children(Node, Children),
    maplist(total_weight, Children, ChildWeights),
    sum_list(ChildWeights, Weight).

total_weight(Node, Weight) :-
    children_weight(Node, ChildrenWeight),
    weight(Node, NodeWeight),
    Weight is NodeWeight + ChildrenWeight.

sibling(Node, Sibling) :-
    child(Node, Parent),
    child(Sibling, Parent),
    Node \== Sibling.

balanced(Node) :-
    sibling(Node, Sibling),
    total_weight(Node, Weight),
    total_weight(Sibling, Weight).

balanced_children(Node) :-
    all_children(Node, Children),
    maplist(total_weight, Children, ChildWeights),
    sort(ChildWeights, SortedChildWeights),
    length(SortedChildWeights, 1).

unbalanced(Node) :-
    child(Node, _),
    balanced_children(Node),
    \+ balanced(Node).

weight_needed(WeightNeeded) :-
    unbalanced(Node),
    sibling(Node, Sibling),
    total_weight(Sibling, SiblingWeight),
    children_weight(Node, ChildrenWeight),
    WeightNeeded is SiblingWeight - ChildrenWeight.

main :-
    load_file("input.txt"),
    weight_needed(WeightNeeded),
    writeln(WeightNeeded).

:- initialization(main, main).
