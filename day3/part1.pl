#!/usr/bin/env swipl -s

:- use_module(library(tabling)).
:- table spiralPos/3.

% root = int(sqrt(n))
% flip = pow(-1, root + 1)

% a = root * (root + 1)
% b = round(root / 2) * flip
% c = a - n

% x =  b + (flip * (c - abs(c)) / 2)
% y = -b + (flip * (c + abs(c)) / 2)

spiralPosition(N, X, Y) :-
    Root is floor(sqrt(N - 1)),
    Flip is -1 ** (Root + 1),
    A is Root * (Root + 1),
    B is round(Root / 2) * Flip,
    C is A - N + 1,
    X is B + (Flip * (C - abs(C)) / 2),
    Y is - B + (Flip * (C + abs(C)) / 2).

spiralDistance(N, Len) :-
    spiralPosition(N, X, Y),
    Len is abs(X) + abs(Y).


decDown(X, Y) :-
    X >= Y,
    X >= -Y + 2.

decUp(X, Y) :-
    X =< Y,
    X =< -Y - 1.

decRight(X, Y) :-
    X >= -Y,
    X =< Y - 1.

decLeft(X, Y) :-
    X >= Y + 1,
    X =< 1 - Y.

spiralPos(1, 0, 0) :- !.

spiralPos(N, X, Y) :-
    M is N - 1,
    spiralPos(M, MX, Y),
    X is MX + 1,
    decLeft(X, Y).

spiralPos(N, X, Y) :-
    M is N - 1,
    spiralPos(M, X, MY),
    Y is MY + 1,
    decDown(X, Y).

spiralPos(N, X, Y) :-
    M is N - 1,
    spiralPos(M, MX, Y),
    X is MX - 1,
    decRight(X, Y).

spiralPos(N, X, Y) :-
    M is N - 1,
    spiralPos(M, X, MY),
    Y is MY - 1,
    decUp(X, Y).
